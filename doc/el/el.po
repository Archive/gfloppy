msgid ""
msgstr ""
"Project-Id-Version: el\n"
"POT-Creation-Date: 2009-03-16 06:30+0000\n"
"PO-Revision-Date: \n"
"Last-Translator: Michael Kotsarinis <mk73628@hotmail.com>\n"
"Language-Team: Greek <team@gnome.gr>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: 2\n"
"X-Poedit-Language: Greek\n"
"X-Poedit-Country: GREECE\n"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: C/gfloppy.xml:197(None)
msgid "@@image: 'figures/main.png'; md5=f89c942f67c8073988ba1a7a818aad69"
msgstr "@@image: 'figures/main.png'; md5=f89c942f67c8073988ba1a7a818aad69"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: C/gfloppy.xml:200(None)
msgid "@@image: 'figures/main.eps'; md5=THIS FILE DOESN'T EXIST"
msgstr "@@image: 'figures/main.eps'; md5=THIS FILE DOESN'T EXIST"

#: C/gfloppy.xml:15(title)
msgid "Floppy Formatter Manual"
msgstr "Εγχειρίδιο Διαμόρφωσης Δισκέτας"

#: C/gfloppy.xml:17(para)
msgid "Floppy Formatter, also known as Gfloppy, formats floppy disks for use in Linux."
msgstr "Η Διαμόρφωση δισκέτας, επίσης γνωστή ως Gfloppy, διαμορφώνει δισκέτες για χρήση στο Linux."

#: C/gfloppy.xml:20(year)
#: C/gfloppy.xml:127(date)
msgid "2000"
msgstr "2000"

#: C/gfloppy.xml:21(holder)
#: C/gfloppy.xml:129(para)
msgid "Kenny Graunke"
msgstr "Kenny Graunke"

#: C/gfloppy.xml:24(year)
msgid "2002"
msgstr "2002"

#: C/gfloppy.xml:25(holder)
msgid "John Fleck"
msgstr "John Fleck"

#: C/gfloppy.xml:28(year)
msgid "2003"
msgstr "2003"

#: C/gfloppy.xml:29(holder)
msgid "Baris Cicek"
msgstr "Baris Cicek"

#: C/gfloppy.xml:41(publishername)
#: C/gfloppy.xml:51(orgname)
#: C/gfloppy.xml:60(orgname)
#: C/gfloppy.xml:68(orgname)
#: C/gfloppy.xml:109(para)
#: C/gfloppy.xml:121(para)
#: C/gfloppy.xml:132(para)
msgid "GNOME Documentation Project"
msgstr "GNOME Documentation Project"

#: C/gfloppy.xml:2(para)
msgid "Permission is granted to copy, distribute and/or modify this document under the terms of the GNU Free Documentation License (GFDL), Version 1.1 or any later version published by the Free Software Foundation with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts. You can find a copy of the GFDL at this <ulink type=\"help\" url=\"ghelp:fdl\">link</ulink> or in the file COPYING-DOCS distributed with this manual."
msgstr "Χορηγείται η άδεια για αντιγραφή, διανομή ή/και τροποποίηση αυτού του εγγράφου κάτω από τους όρους του GNU·Free·Documentation·License·(GFDL) έκδοσης 1.1 ή οποιαδήποτε άλλης έκδοσης δημοσιευμένης από το Free·Software·Foundation με αναλλοίωτα στοιχεία, χωρίς κείμενα Front-Cover και κείμενα Back-Cover. Μπορείτε να βρείτε ένα αντίγραφο του GFDL στο σύνδεσμο <ulink·type=\"help\"·url=\"ghelp:fdl\">link</ulink> ή στο αρχείο COPYING-DOCS που διανέμεται με αυτόν τον οδηγό."

#: C/gfloppy.xml:12(para)
msgid "This manual is part of a collection of GNOME manuals distributed under the GFDL. If you want to distribute this manual separately from the collection, you can do so by adding a copy of the license to the manual, as described in section 6 of the license."
msgstr "Το εγχειρίδιο αυτό είναι τμήμα της συλλογής εγχειριδίων GNOME που διανέμεται υπό την άδεια GFDL. Αν επιθυμείτε να διανείμετε το παρόν εγχειρίδιο χωριστά από την συλλογή, μπορείτε να το κάνετε προσθέτοντας ένα αντίγραφο της άδειας στο εγχειρίδιο, όπως περιγράφεται στο τμήμα 6 της άδειας."

#: C/gfloppy.xml:19(para)
msgid "Many of the names used by companies to distinguish their products and services are claimed as trademarks. Where those names appear in any GNOME documentation, and the members of the GNOME Documentation Project are made aware of those trademarks, then the names are in capital letters or initial capital letters."
msgstr "Πολλά ονόματα που χρησιμοποιούν οι εταιρίες για να διακρίνουν μεταξύ τους προϊόντα και υπηρεσίες διεκδικούνται ως εμπορικά σήματα. Όπου τα ονόματα αυτά εμφανίζονται σε οποιαδήποτε τεκμηρίωση GNOME, και τα μέλη του Σχεδίου Τεκμηρίωσης GNOME είναι ενήμερα αυτών των εμπορικών σημάτων, τότε τα ονόματά τους εμφανίζονται με κεφαλαία γράμματα ή με τα αρχικά κεφαλαία γράμματα."

#: C/gfloppy.xml:35(para)
msgid "DOCUMENT IS PROVIDED ON AN \"AS IS\" BASIS, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, WITHOUT LIMITATION, WARRANTIES THAT THE DOCUMENT OR MODIFIED VERSION OF THE DOCUMENT IS FREE OF DEFECTS MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE OR NON-INFRINGING. THE ENTIRE RISK AS TO THE QUALITY, ACCURACY, AND PERFORMANCE OF THE DOCUMENT OR MODIFIED VERSION OF THE DOCUMENT IS WITH YOU. SHOULD ANY DOCUMENT OR MODIFIED VERSION PROVE DEFECTIVE IN ANY RESPECT, YOU (NOT THE INITIAL WRITER, AUTHOR OR ANY CONTRIBUTOR) ASSUME THE COST OF ANY NECESSARY SERVICING, REPAIR OR CORRECTION. THIS DISCLAIMER OF WARRANTY CONSTITUTES AN ESSENTIAL PART OF THIS LICENSE. NO USE OF ANY DOCUMENT OR MODIFIED VERSION OF THE DOCUMENT IS AUTHORIZED HEREUNDER EXCEPT UNDER THIS DISCLAIMER; AND"
msgstr "ΤΟ ΠΑΡΟΝ ΕΓΓΡΑΦΟ ΠΑΡΕΧΕΤΑΙ \"ΩΣ ΕΧΕΙ\", ΧΩΡΙΣ ΟΠΟΙΑΔΗΠΟΤΕ ΕΓΓΥΗΣΗ, ΕΚΠΕΦΡΑΣΜΕΝΗ Ή ΥΠΑΙΝΙΣΣΟΜΕΝΗ, ΣΥΜΠΕΡΙΛΑΜΒΑΝΟΜΕΝΩΝ, ΧΩΡΙΣ ΠΕΡΙΟΡΙΣΜΟΥΣ, ΕΓΓΥΗΣΕΩΝ ΟΤΙ ΤΟ ΕΓΓΡΑΦΟ Ή Η ΤΡΟΠΟΠΟΙΗΜΕΝΗ ΕΚΔΟΣΗ ΤΟΥ ΕΙΝΑΙ ΑΠΑΛΑΓΜΕΝΟ ΑΤΕΛΕΙΩΝ, ΕΜΠΟΡΕΥΣΙΜΟ, ΚΑΤΑΛΛΗΛΟ ΓΙΑ ΣΥΓΚΕΚΡΙΜΕΝΗ ΧΡΗΣΗ Ή ΑΠΑΡΑΒΙΑΣΤΟ. Ο ΧΡΗΣΤΗΣ ΑΝΑΛΑΜΒΑΝΕΙ ΚΑΘΕ ΚΙΝΔΥΝΟ ΠΟΥ ΜΠΟΡΕΙ ΝΑ ΠΡΟΚΥΨΕΙ ΩΣ ΠΡΟΣ ΤΗΝ ΠΟΙΟΤΗΤΑ, ΑΚΡΙΒΕΙΑ ΚΑΙ ΛΕΙΤΟΥΡΓΙΑ ΤΟΥ ΕΓΓΡΑΦΟΥ Ή ΤΩΝ ΤΡΟΠΟΠΟΙΗΜΕΝΩΝ ΕΚΔΟΣΕΩΝ ΤΟΥ. ΑΝ ΟΠΟΙΟΔΗΠΟΤΕ ΕΓΓΡΑΦΟ Ή ΤΡΟΠΟΠΟΙΗΜΕΝΗ ΕΚΔΟΣΗ ΤΟΥ ΑΠΟΔΕΙΧΘΟΥΝ ΕΛΛΑΤΩΜΑΤΙΚΑ ΜΕ ΟΠΟΙΟΝΔΗΠΟΤΕ ΤΡΟΠΟ, Ο ΤΕΛΙΚΟΣ ΧΡΗΣΤΗΣ (ΚΑΙ ΟΧΙ Ο ΑΡΧΙΚΟΣ ΣΥΓΓΡΑΦΕΑΣ, ΣΥΝΤΑΚΤΗΣ Ή ΟΠΟΙΟΣΔΗΠΟΤΕ ΤΟ ΔΙΕΝΗΜΕ) ΑΝΑΛΑΜΒΑΝΕΙ ΤΟ ΚΟΣΤΟΣ ΟΠΟΙΑΣΔΗΠΟΤΕ ΕΠΙΣΚΕΥΗΣ Ή ΔΙΟΡΘΩΣΗΣ. ΑΥΤΗ Η ΠΑΡΑΙΤΗΣΗ ΑΠΟ ΕΓΓΥΗΤΙΚΕΣ ΕΥΘΥΝΕΣ ΣΥΝΙΣΤΑ ΟΥΣΙΑΣΤΙΚΟ ΜΕΡΟΣ ΤΗΣ ΑΔΕΙΑΣ. ΟΥΔΕΜΙΑ ΧΡΗΣΗ ΟΙΟΥΔΗΠΟΤΕ ΕΓΓΡΑΦΟΥ Ή ΤΡΟΠΟΠΟΙΗΜΕΝΟΥ ΕΓΓΡΑΦΟΥ ΕΞΟΥΣΙΟΔΟΤΕΙΤΑΙ ΕΦΕΞΗΣ, ΠΑΡΑ ΥΠΟ ΤΟΝ ΟΡΟ ΤΗΣ ΩΣ ΑΝΩ ΠΑΡΑΙΤΗΣΗΣ ΚΑΙ"

#: C/gfloppy.xml:55(para)
msgid "UNDER NO CIRCUMSTANCES AND UNDER NO LEGAL THEORY, WHETHER IN TORT (INCLUDING NEGLIGENCE), CONTRACT, OR OTHERWISE, SHALL THE AUTHOR, INITIAL WRITER, ANY CONTRIBUTOR, OR ANY DISTRIBUTOR OF THE DOCUMENT OR MODIFIED VERSION OF THE DOCUMENT, OR ANY SUPPLIER OF ANY OF SUCH PARTIES, BE LIABLE TO ANY PERSON FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES OF ANY CHARACTER INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF GOODWILL, WORK STOPPAGE, COMPUTER FAILURE OR MALFUNCTION, OR ANY AND ALL OTHER DAMAGES OR LOSSES ARISING OUT OF OR RELATING TO USE OF THE DOCUMENT AND MODIFIED VERSIONS OF THE DOCUMENT, EVEN IF SUCH PARTY SHALL HAVE BEEN INFORMED OF THE POSSIBILITY OF SUCH DAMAGES."
msgstr "ΣΕ ΚΑΜΙΑ ΠΕΡΙΠΤΩΣΗ ΚΑΙ ΣΕ ΚΑΜΙΑ ΝΟΜΟΛΟΓΙΑ ΑΣΤΙΚΟΥ ΑΔΙΚΗΜΑΤΟΣ (ΠΕΡΙΛΑΜΒΑΝΟΜΕΝΗΣ ΕΚΕΙΝΟΥ ΤΗΣ ΑΜΕΛΕΙΑΣ), ΣΥΜΒΟΛΑΙΟΥ Ή ΑΛΛΟΥ, Ο ΣΥΝΤΑΚΤΗΣ, Ο ΑΡΧΙΚΟΣ ΣΥΓΓΡΑΦΕΑΣ, ΟΠΟΙΟΣΔΗΠΟΤΕ ΣΥΝΕΡΓΑΤΗΣ Ή ΟΠΟΙΟΣΔΗΠΟΤΕ ΔΙΑΝΟΜΕΑΣ ΤΟΥ ΕΓΓΡΑΦΟΥ Ή ΤΡΟΠΟΠΟΙΗΜΕΝΗΣ ΕΚΔΟΣΗΣ ΤΟΥ ΕΓΓΡΑΦΟΥ, Ή ΟΠΟΙΣΔΗΠΟΤΕ ΠΡΟΜΗΘΕΥΤΗΣ ΟΠΟΙΟΥΔΗΠΟΤΕ ΕΚ ΤΩΝ ΑΝΩΤΕΡΩ ΠΡΟΣΩΠΩΝ ΕΙΝΑΙ ΥΠΕΥΘΥΝΟΣ ΕΝΑΝΤΙ ΟΙΟΥΔΗΠΟΤΕ ΠΡΟΣΩΠΟΥ ΓΙΑ ΟΠΟΙΑΔΗΠΟΤΕ ΑΜΕΣΗ, ΕΜΜΕΣΗ, ΙΔΙΑΙΤΗΕΡΗ, ΑΤΥΧΗ Ή ΣΥΝΕΠΑΓΟΜΕΝΗ ΒΛΑΒΗ ΟΙΟΥΔΗΠΟΤΕ ΧΑΡΑΚΤΗΡΑ ΣΥΜΠΕΡΙΛΑΜΒΑΝΟΜΕΝΩΝ, ΧΩΡΙΣ ΠΕΡΙΟΡΙΣΜΟΥΣ, ΒΛΑΒΕΣ ΕΞΑΙΤΙΑΣ ΑΠΩΛΕΙΑΣ ΠΕΛΑΤΕΙΑΣ, ΣΤΑΣΗ ΕΡΓΑΣΙΑΣ, ΑΣΤΟΧΙΑ Ή ΚΑΚΗ ΛΕΙΤΟΥΡΓΙΑ ΥΠΟΛΟΓΙΣΤΗ, Ή ΟΠΟΙΑΔΗΠΟΤΕ ΚΑΙ ΚΑΘΕ ΑΛΛΗ ΒΛΑΒΗ Ή ΑΠΩΛΕΙΑ ΠΟΥ ΘΑ ΕΓΕΙΡΕΙ Ή ΣΧΕΤΙΖΕΤΑΙ ΜΕ ΤΗΝ ΧΡΗΣΗ ΤΟΥ ΕΓΓΡΑΦΟΥ ΚΑΙ ΟΠΟΙΑΣΔΗΠΟΤΕ ΤΡΟΠΟΠΟΙΗΜΕΝΗΣ ΕΚΔΟΣΗΣ ΤΟΥ ΕΓΓΡΑΦΟΥ, ΑΚΟΜΗ ΚΑΙ ΑΝ ΚΑΠΟΙΟ ΑΠΟ ΑΥΤΑ ΤΑ ΠΡΟΣΩΠΑ ΕΙΝΑΙ ΕΝΗΜΕΡΟ ΓΙΑ ΤΗΝ ΠΙΘΑΝΟΤΗΤΑ ΠΡΟΚΛΗΣΗΣ ΤΕΤΟΙΩΝ ΒΛΑΒΩΝ."

#: C/gfloppy.xml:28(para)
msgid "DOCUMENT AND MODIFIED VERSIONS OF THE DOCUMENT ARE PROVIDED UNDER THE TERMS OF THE GNU FREE DOCUMENTATION LICENSE WITH THE FURTHER UNDERSTANDING THAT: <placeholder-1/>"
msgstr "ΤΟ ΕΓΓΡΑΦΟ ΚΑΙ ΟΙ ΤΡΟΠΟΠΟΙΗΜΕΝΕΣ ΕΚΔΟΣΕΙΣ ΤΟΥ ΕΓΓΡΑΦΟΥ ΠΑΡΕΧΟΝΤΑΙ ΥΠΟ ΤΟΥΣ ΟΡΟΥΣ ΤΗΣ ΕΛΕΥΘΕΡΗΣ ΑΔΕΙΑΣ ΤΕΚΜΗΡΙΩΣΗΣ GNU ΜΕ ΤΗΝ ΠΕΡΑΙΤΕΡΩ ΚΑΤΑΝΟΗΣΗ ΟΤΙ: <placeholder-1/>"

#: C/gfloppy.xml:48(firstname)
msgid "Baris"
msgstr "Baris"

#: C/gfloppy.xml:49(surname)
msgid "Cicek"
msgstr "Cicek"

#: C/gfloppy.xml:52(email)
msgid "bcicek@phreaker.net"
msgstr "bcicek@phreaker.net"

#: C/gfloppy.xml:57(firstname)
msgid "John"
msgstr "John"

#: C/gfloppy.xml:58(surname)
msgid "Fleck"
msgstr "Fleck"

#: C/gfloppy.xml:61(email)
msgid "jfleck@inkstain.net"
msgstr "jfleck@inkstain.net"

#: C/gfloppy.xml:65(firstname)
msgid "Kenny"
msgstr "Kenny"

#: C/gfloppy.xml:66(surname)
msgid "Graunke"
msgstr "Graunke"

#: C/gfloppy.xml:103(revnumber)
msgid "Gfloppy Manual V2.1"
msgstr "Gfloppy Manual V2.1"

#: C/gfloppy.xml:104(date)
msgid "January 2003"
msgstr "January 2003"

#: C/gfloppy.xml:106(para)
msgid "Baris Cicek <email>bcicek@phreaker.net</email>"
msgstr "Baris Cicek <email>bcicek@phreaker.net</email>"

#: C/gfloppy.xml:114(revnumber)
msgid "Gfloppy Manual V4.0"
msgstr "Gfloppy Manual V4.0"

#: C/gfloppy.xml:115(date)
msgid "June 2002"
msgstr "June 2002"

#: C/gfloppy.xml:118(para)
msgid "John Fleck <email>jfleck@inkstain.net</email>"
msgstr "John Fleck <email>jfleck@inkstain.net</email>"

#: C/gfloppy.xml:126(revnumber)
msgid "Gfloppy Manual"
msgstr "Εγχειρίδιο του Gfloppy"

#: C/gfloppy.xml:137(releaseinfo)
msgid "This manual describes version 2.4 of Gfloppy Manual"
msgstr "Αυτό το εγχειρίδιο περιγράφει την έκδοση 2.4 του Gfloppy"

#: C/gfloppy.xml:140(title)
msgid "Feedback"
msgstr "Ανάδραση"

#: C/gfloppy.xml:141(para)
msgid "To report a bug or make a suggestion regarding the Gfloppy application or this manual, follow the directions in the <ulink url=\"ghelp:gnome-feedback\" type=\"help\">GNOME Feedback Page</ulink>."
msgstr "Για να αναφέρετε ένα σφάλμα ή να κάνετε μια πρόταση σχετικά με την εφαρμογή Gfloppy ή αυτό το εγχειρίδιο, ακολουθήστε τις οδηγίες στο <ulink url=\"ghelp:gnome-feedback\" type=\"help\">GNOME Feedback Page</ulink>."

#: C/gfloppy.xml:160(title)
msgid "Introduction"
msgstr "Εισαγωγή"

#: C/gfloppy.xml:162(para)
msgid "<application>GFloppy</application> formats floppy disks for Linux. New floppy disks might need to be formatted before they can be used."
msgstr "Το <application>GFloppy</application> διαμορφώνει δισκέτες για χρήση στο Linux. Οι καινούριες δισκέτες μπορεί να χρειασθούν διαμόρφωση πριν τη χρήση."

#: C/gfloppy.xml:167(para)
msgid "To run <application>GFloppy</application>, select <guimenuitem>Floppy Formatter</guimenuitem> from the <guisubmenu>System Tools</guisubmenu> submenu of the <guimenu>Main Menu</guimenu>, or type <command>gfloppy</command> on the command line."
msgstr "Για να εκτελέσετε το <application>GFloppy</application>, επιλέξτε <guimenuitem>Διαμόρφωση δισκέτας</guimenuitem> από το υπομενού <guisubmenu>Εργαλεία συστήματος</guisubmenu> στο <guimenu>Κεντρικό μενού</guimenu> ή πληκτρολογήστε <command>gfloppy</command> στη γραμμή εντολών."

#: C/gfloppy.xml:176(para)
msgid "This document describes version 2.4 of <application>GFloppy</application>."
msgstr "Αυτό το έγγραφο περιγράφει την έκδοση 2.4 του <application>GFloppy</application>."

#: C/gfloppy.xml:187(title)
msgid "Use of GFloppy"
msgstr "Χρήση του GFloppy"

#: C/gfloppy.xml:192(title)
msgid "Gfloppy Main Window"
msgstr "Κεντρικό παράθυρο του Gfloppy"

#: C/gfloppy.xml:194(screeninfo)
#: C/gfloppy.xml:203(phrase)
#: C/gfloppy.xml:205(para)
msgid "GFloppy Main Window"
msgstr "Κεντρικό παράθυρο του Gfloppy"

#: C/gfloppy.xml:188(para)
msgid "When you start <application>GFloppy</application>, the <interface>Main window</interface> opens, shown in <xref linkend=\"gfloppymainwindow\"/>. <placeholder-1/>"
msgstr "Όταν εκκινείτε το <application>GFloppy</application>, ανοίγει το <interface>Κεντρικό παράθυρο</interface>, που φαίνεται στο <xref linkend=\"gfloppymainwindow\"/>. <placeholder-1/>"

#: C/gfloppy.xml:212(para)
msgid "Insert a floppy disk that is not write-protected."
msgstr "Εισάγετε μια δισκέτα που δεν είναι προστατευμένη από εγγραφή."

#: C/gfloppy.xml:217(title)
msgid "Warning"
msgstr "Προειδοποίηση"

#: C/gfloppy.xml:218(para)
msgid "GFloppy will destroy all files on your floppy disk. Do not format floppies containing files you wish to keep."
msgstr "Το GFloppy θα καταστρέψει όλα τα αρχεία στη δισκέτα σας. Μην διαμορφώνετε δισκέτες που περιέχουν αρχεία που επιθυμείτε να κρατήσετε."

#: C/gfloppy.xml:223(para)
msgid "GFloppy needs to know the density of your floppy disk, the amount of data it can hold. Most disks today are High Density, 1.44MB 3.5\"."
msgstr "Το GFloppy πρέπει να γνωρίζει τη χωρητικότητα της δισκέτας σας, τον όγκο δεδομένων που μπορεί να χωρέσει. Οι περισσότερες δισκέτες σήμερα είναι High Density, 1.44MB 3.5\"."

#: C/gfloppy.xml:228(para)
msgid "Select your file system type. If you need to use your floppy under DOS or Windows, choose <guibutton>DOS (FAT)</guibutton>. Otherwise, choose <guibutton>Linux Native (ext2)</guibutton>. Linux Native cannot be read on most non-Linux machines."
msgstr "Επιλέξτε τον τύπο του συστήματος αρχείων σας. Αν πρέπει να χρησιμοποιήσετε τη δισκέτα σας σε περιβάλλον DOS ή Windows, επιλέξετε <guibutton>DOS (FAT)</guibutton>. Αλλιώς, επιλέξτε <guibutton>Linux Native (ext2)</guibutton>. Η διαμόρφωση Linux Native δεν μπορεί να αναγνωσθεί από τα περισσότερα μη-Linux συστήματα."

#: C/gfloppy.xml:234(para)
msgid "You can also give <guibutton>Volume name</guibutton> to your newly formatted floppies. To do this type your label in the text box under <guibutton>Filesystem Settings</guibutton>."
msgstr "Μπορείτε επίσης να δώσετε <guibutton>Όνομα τόμου</guibutton> στις νεοδιαμορφωμένες δισκέτες σας. Για να το κάνετε αυτό, πληκτρολογήστε την ετικέτα σας στο πεδίο κειμένου κάτω από το <guibutton>Ρυθμίσεις συστήματος αρχείων</guibutton>."

#: C/gfloppy.xml:243(guibutton)
msgid "Quick"
msgstr "Γρήγορη"

#: C/gfloppy.xml:245(para)
msgid "Quick format is fastest but does not check your floppy for bad \"blocks\". It just deletes the file table so your data is theoretically recoverable."
msgstr "Η γρήγορη διαμόρφωση είναι η γρηγορότερη αλλά δεν ελέγχει τη δισκέτα σας για κατεστραμμένα  \"μπλοκ\". Απλώς διαγράφει τον πίνακα αρχείων έτσι τα δεδομένα σας, θεωρητικά, είναι ανακτήσιμα."

#: C/gfloppy.xml:251(guibutton)
msgid "Standard"
msgstr "Τυπική"

#: C/gfloppy.xml:253(para)
msgid "Standard format is a low-level format, but it does not check bad blocks. Thus if you have a faulty floppy it will fail to format."
msgstr "Η τυπική διαμόρφωση είναι διαμόρφωση σε χαμηλό επίπεδο, αλλά δεν ελέγχει για κατεστραμμένα μπλοκ. Έτσι, αν έχετε μια ελλαττωματική δισκέτα δεν θα μπορέσει να διαμορφωθεί."

#: C/gfloppy.xml:259(guibutton)
msgid "Thorough"
msgstr "Πλήρης"

#: C/gfloppy.xml:261(para)
msgid "Thorough format checks floppy for bad blocks and low-level format it."
msgstr "Η πλήρης διαμόρφωση ελέγχει τη δισκέτα για κατεστραμμένα μπλοκ και κάνει διαμόρφωση χαμηλού επιπέδου."

#: C/gfloppy.xml:240(para)
msgid "Finally, you should select the mode of your formatting. <placeholder-1/>"
msgstr "Τέλος, θα πρέπει να επιλέξετε τη λειτουργία της διαμόρφωσής σας. <placeholder-1/>"

#. <para>
#.     Quick format is faster but does not check your floppy disk for bad
#.     "blocks". The slower regular format is useful if you have not formatted your
#.     floppy before, or if you suspect your disk might be bad.
#.    </para>
#: C/gfloppy.xml:275(para)
msgid "Click on the <guibutton>Format</guibutton> button to start formatting your floppy disk, as shown in these figures."
msgstr "Κάντε κλικ στο κουμπί <guibutton>Διαμόρφωση</guibutton> για να αρχίσετε τη διαμόρφωση της δισκέτας σας, όπως φάινεται σε αυτές τις εικόνες."

#: C/gfloppy.xml:280(para)
msgid "You will see a series of dialogs informing you that <application>Gfloppy</application> is formatting, verifying and making a filesystem on your disk."
msgstr "Θα δείτε μια σειρά παραθύρων διαλόγου που σας πληροφορούν ότι το <application>Gfloppy</application> διαμορφώνει, επαληθεύει και φτιάχνει ένα σύστημα αρχείων στη δισκέτα σας."

#: C/gfloppy.xml:284(para)
msgid "Unless you select <guibutton>Quick Format</guibutton> or <guibutton>Standard</guibutton>, <application>GFloppy</application> checks for bad \"blocks\" on your floppy. If <application>Gfloppy</application> finds any, your floppy is wearing out."
msgstr "Αν δεν επιλέξετε τη <guibutton>Γρήγορη διαμόρφωση</guibutton> ή την <guibutton>Τυπική</guibutton>, το <application>GFloppy</application> ελέγχει για κατεστραμμένα \"μπλοκ\" στη δισκέτα σας. Αν το <application>Gfloppy</application> βρει κάποιο, σημαίνει ότι η δισκέτα σας είναι φθαρμένη."

#: C/gfloppy.xml:292(para)
msgid "<application>GFloppy</application> will let you know if it was successful and it will return you to the main window."
msgstr "Το <application>GFloppy</application> θα σας ειδοποιήσει αν ήταν επιτυχής η διαμόρφωση και θα επιστρέψει στο κεντρικό παράθυρο."

#: C/gfloppy.xml:300(title)
msgid "Troubleshooting"
msgstr "Αντιμετώπιση προβλημάτων"

#: C/gfloppy.xml:301(para)
msgid "When no floppy disk is inserted, GFloppy displays a dialog informing you that there's no media to be formated."
msgstr "Όταν δεν έχει εισαχθεί δισκέτα, το GFloppy εμφανίζει ένα παράθυρο διαλόγου που σας πληροφορεί ότι δεν υπάρχει μέσο προς διαμόρφωση."

#: C/gfloppy.xml:305(para)
msgid "If your disk is write protected, GFloppy will warn you as well. To un-write protect your floppy, eject it and turn it over. Standard 3.5 inch floppies have a small black tab in the upper-left corner. Slide it down so it covers the hole. If the floppy does not have a black tab, it is permanently write protected."
msgstr "Αν η δισκέτα σας έχει προστασία εγγραφής, το GFloppy θα σας προειδοπιοήσει σχετικά. Για να άρετε την προστασία εγγραφής, βγάλτε τη και γυρίστε τη ανάποδα. Οι τυπικές δισκέτες των 3,5 ιντσών έχουν ένα μικρό σύρτη στη μία γωνία. Σύρετέ τον ώστε να καλύπτει την τρύπα. Αν η δισκέτα δεν έχει τέτοιο σύρτη είναι μόνιμα προστατευμένη από εγγραφή."

#: C/gfloppy.xml:368(title)
msgid "Known Bugs and Limitations"
msgstr "Γνωστά προβλήματα και περιορισμοί"

#: C/gfloppy.xml:369(para)
msgid "GFloppy only works on Linux."
msgstr "Το GFloppy δουλεύει μόνο σε Linux."

#: C/gfloppy.xml:377(title)
msgid "Authors"
msgstr "Συγγραφείς"

#: C/gfloppy.xml:378(para)
msgid "<application>GFloppy</application> was written by Jonathan Blandford (<email>jrb@redhat.com</email>). Please send all comments, suggestions, and bug reports to the <ulink url=\"http://bugzilla.gnome.org\" type=\"http\"> GNOME bug tracking database</ulink>. If you are using GNOME 1.1 or later, you can also use <application>Bug Report Tool</application> (<command>bug-buddy</command>), available in the <guisubmenu>Programming</guisubmenu> submenu of <guimenu>Main Menu</guimenu>, for submitting bug reports."
msgstr "Το <application>GFloppy</application> γράφτηκε από τον Jonathan Blandford (<email>jrb@redhat.com</email>). Παρακαλούμε στείλτε όλα τα σχόλια, προτάσεις και αναφορές σφαλμάτων στο <ulink url=\"http://bugzilla.gnome.org\" type=\"http\"> GNOME bug tracking database</ulink>. Αν χρησιμοποιείτε GNOME 1.1 ή μεταγενέστερο, μπορείτε επίσης να χρησιμοποιήσετε το <application>Εργαλείο αναφοράς σφάλμταος</application> (<command>bug-buddy</command>), που βρίσκεται στο υπομενού <guisubmenu>Προγραμματισμός</guisubmenu> στο <guimenu>Κεντρικό μενού</guimenu>, για να υποβάλετε αναφορές σφαλμάτων."

#: C/gfloppy.xml:389(para)
msgid "This manual was written by Baris Cicek (<email>bcicek@phreaker.net</email>). Please send all comments and suggestions regarding this manual to the <ulink type=\"http\" url=\"http://developer.gnome.org/projects/gdp/\">GNOME Documentation Project</ulink> by sending an email to <email>docs@gnome.org</email>. You can also add your comments online by using the <ulink type=\"http\" url=\"http://developer.gnome.org/projects/gdp/doctable/\">GNOME Documentation Status Table</ulink>."
msgstr "Αυτό το εγχειρίδιο γράφτηκε από τον  Baris Cicek (<email>bcicek@phreaker.net</email>).  Παρακαλούμε στείλτε όλα τα σχόλια και προτάσεις σχετικά με αυτό το εγχειρίδιο στο <ulink type=\"http\" url=\"http://developer.gnome.org/projects/gdp/\">GNOME Documentation Project</ulink> στέλνοντας ένα email στο <email>docs@gnome.org</email>. Μπορείτε επίσης να προσθέσετε τα σχόλιά σας online χρησιμοποιώντας το <ulink type=\"http\" url=\"http://developer.gnome.org/projects/gdp/doctable/\">GNOME Documentation Status Table</ulink>."

#: C/gfloppy.xml:416(title)
msgid "License"
msgstr "Άδεια"

#: C/gfloppy.xml:417(para)
msgid "This program is free software; you can redistribute it and/or modify it under the terms of the <ulink url=\"ghelp:gpl\"><citetitle>GNU General Public License</citetitle></ulink> as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version."
msgstr "Αυτό το πρόγραμμα είνια ελεύθερο λογισμικό. Μπορείτε να το αναδιανείμετε και/ή να το τροποποιήσετε υπό τους όρους της <ulink url=\"ghelp:gpl\"><citetitle>GNU General Public License</citetitle></ulink> όπως δημοσιεύεται από το Free Software Foundation, είτε υπό την έκδοση 2 της άδειας ή (κατ' επιλογή σας) υπό οποιαδήποτε μεταγενέστερη έκδοση."

#: C/gfloppy.xml:424(para)
msgid "This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the <ulink url=\"ghelp:gpl\"><citetitle>GNU General Public License</citetitle></ulink> for more details."
msgstr "Αυτό το πρόγραμμα διανέμεται με την πεποίθηση ότι θα είναι χρήσιμο αλλά ΧΩΡΙΣ ΚΑΜΜΙΑ ΕΓΓΥΗΣΗ. Ακόμα και χωρίς καμμία υπαινισσόμενη εγγύηση ΕΜΠΟΡΕΥΣΙΜΟΤΗΤΑΣ ή ΚΑΤΑΛΛΗΛΟΤΗΤΑΣ ΓΙΑ ΣΥΓΚΕΚΡΙΜΕΝΟ ΣΚΟΠΟ. Βλέπε το <ulink url=\"ghelp:gpl\"><citetitle>GNU General Public License</citetitle></ulink> για περισσότερες λεπτομέρειες."

#: C/gfloppy.xml:431(para)
msgid "A copy of the <citetitle>GNU General Public License</citetitle> is included as an appendix to the <citetitle>GNOME Users Guide</citetitle>. You may also obtain a copy of the <citetitle>GNU General Public License</citetitle> from the Free Software Foundation by visiting <ulink type=\"http\" url=\"http://www.fsf.org\">their Web site</ulink> or by writing to <address> Free Software Foundation, Inc. <street>59 Temple Place</street> - Suite 330 <city>Boston</city>, <state>MA</state><postcode>02111-1307</postcode><country>USA</country></address>"
msgstr "Ένα αντίγραφο της <citetitle>GNU General Public License</citetitle> περιλαμβάνεται ως παράρτημα στον <citetitle>GNOME Users Guide</citetitle>. Μπορείτε επίσης να αποκτήσετε ένα αντίγραφο της <citetitle>GNU General Public License</citetitle> από το  Free Software Foundation επισκεπτόμενοι την ιστοσελίδα τους <ulink type=\"http\" url=\"http://www.fsf.org\"> ή γράφοντας στη διεύθυνση <address> Free Software Foundation, Inc. <street>59 Temple Place</street> - Suite 330 <city>Boston</city>, <state>MA</state><postcode>02111-1307</postcode><country>USA</country></address>."

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2.
#: C/gfloppy.xml:0(None)
msgid "translator-credits"
msgstr "Michael Kotsarinis, 2009"

